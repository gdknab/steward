#!/bin/bash

sudo apt-get update

# Install the babel routing daemon, mtr fancy traceroute, and nodejs (for the babelweb node)
sudo apt-get -y install babeld mtr npm nodejs-legacy

# Get a list of all experiment interfaces
ifaces=$(netstat -i | tail -n+3 | grep -v "lo" | grep -v "eth0" | cut -d' ' -f1 )

# remove InstaGENI-generated automatic routes
for i in $ifaces; do 
    sudo ifconfig $i down
    sudo ifconfig $i up
done

ifacelist=$(echo $ifaces)
sudo babeld -g 33123 -D -d 1 -C 'redistribute local if eth0 deny' $ifacelist

mkdir Documents
cd Documents
git clone https://github.com/SXibolet/Steward
cd Steward
mkdir bin/keys
cd src
make
cd ../bin
./gen_keys
